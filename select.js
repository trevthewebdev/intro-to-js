'use strict';

(function(App) {

  /* Select Constructor Function */
  function Select($elem, userOptions) {
    if (!$elem.id) {
      throw 'You need to have an id on your select container';
    }

    if (!_areValiduserOptions(userOptions)) {
      throw 'Invalid Config: Please check your config and try again';
    }

    this.$container = $elem;

    // overwrite defaults with user passed options
    this.options = Object.assign({}, Select.defaults, {
      containerClass: userOptions.classes,
      label: userOptions.label
    });

    // set this select instance's state
    this.state = {
      isOpen: userOptions.isDisabled || false,
      isDisabled: userOptions.isDisabled || false,
      options: userOptions.options || [],
      selected: userOptions.selected || ''
    };
  }

  Select.prototype = {
    defaults: {
      containerClass: 'Select',
      label: ''
    },

    /* function that initializes a new Select input */
    init: function() {
      // initial render
      this.render();

      this.gatherElements();
      this.bindEvents();
    },

    /* function that renders the html */
    render: function() {

    },

    /* function that saves almost all the elements we need to make changes to */
    gatherElements: function () {

    },

    /* function that sets up all event listeners for this */
    bindEvents: function() {

    }
  };


  /* attaches a function that creates new Select components based on an element and passed in options */
  App.Select = function makeSelect($elems, userOptions) {
    var $elem = $elems;

    if (Array.isArray$elems.length) {
      $elem = $elems[0];
    }

    return new Select($elem, userOptions);
  }



  /* checks if option label && value are valid */
  function _verifyUserOptions() {
    var isValid = true;

    if ( !Array.isArray(userOptions.options) ) {
      isValid = false;
    }

    if ( userOptions.options.length ) {

      /* loop over the options and check if their valid */
      userOptions.options.forEach(function(option, index) {
        if (!_isValidOption(option)) {
          throw `option: ${option} at index ${index} is not a valid "Select" option`;
          isValid = false;
        }
      });
    }

    return isValid;
  }

  /* check is an option is an object, has a lable & value that are strings, and the value cannot have spaces */
  function _isValidOption(option) {
    if (
      typeof option === 'object' &&
      typeof option.label === 'string' &&
      typeof option.value === 'string' &&
      !option.value.match(/ /g)
    ) {
      return true;
    }
  }

})(window.App = window.App || {});

