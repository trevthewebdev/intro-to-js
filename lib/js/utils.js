'use strict';

function randomNum(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function dispatch(eventName, details) {
  var event = new CustomEvent(eventName, { detail: details });
  window.dispatchEvent(event);
}
