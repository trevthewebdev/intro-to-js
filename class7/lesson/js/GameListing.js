'use strict';

(function (App) {

  /* export this component to be used elsewhere */
  App.GameListing = {
    hasListing: false,

    init: function init() {
      this.$container = document.getElementById('game-list');
      this.bindEvents();
    },

    render: function render(html) {
      this.$container.innerHTML = '';
      this.$container.appendChild(html);
    },

    bindEvents: function bindEvents() {
      var self = this;
      var $starButtons = document.querySelectorAll('button[data-game-id]');

      window.addEventListener('/app/listing/update', function() {
        var games = App.getState('GameListing').games;
        var html = _generateListHtml(games);

        /* visually update the listing */
        self.render(html);
      });

      this.$container.addEventListener('click', function (event) {
        // did we click on the button? if not we probably clicked on the i.fa so go up to the button
        var $elem = (
          event.target.dataset.gameId
            ? event.target
            : event.target.parentNode
        );
        var gameId = $elem.dataset.gameId;

        // bail early because the user didn't click on the star button
        if (!gameId) {
          return;
        }

        var state = App.getState('GameSelections');

        // make a copy of the selections array in the state
        var selections = state.selections.slice();

        // see if the id already exists in the array
        var duplicate = selections.find(function(id) {
          if (id === gameId) {
            return id;
          }
        });

        // if the count of the selections are less than the max (3) add it to the array
        if (!duplicate && state.selections.length < state.maximum) {
          selections.push(gameId);

          // dispatch a custom event and send the game id from the element we clicked on
          App.setState({
            GameSelections: { selections: selections }
          });

          // tell the whole app about the update
          dispatch('/app/selections/update');
        }
      });
    },
  };

  /* _generateListHtml: will take an array of games and generate the listing in html */
  function _generateListHtml(gamesArray) {
    var $fragment = document.createDocumentFragment();

    gamesArray.forEach(function (game, index) {
      var $li = document.createElement('li');

      $li.classList.add('cell', 'medium-4', 'large-3');
      $li.innerHTML = (`
        <div class="card--game">
          <figure>
            <img src="https://placehold.it/400x250" alt="" />
          </figure>
          <div class="card__content">
            <h6 class="card__title">${game.name}</h6>
            <p>${game.description || 'Spooky scarey skeletons are doing things in their sleep...'}</p>
            <ul class="no-bullet">
              <li>${_formatTimeText(game.min_duration, game.max_duration)}</li>
              <li>${_formatPlayerText(game.player_min, game.player_max)}</li>
            </ul>
          </div>
          <div class="card__circle-actions">
            <button data-game-id="${game._id}">
              <i class="fa"></i>
            </button>
          </div>
        </div>
      `);

      $fragment.appendChild($li);
    });

    return $fragment;
  }

  function _formatTimeText(minDuration, maxDuration) {
    return (minDuration === maxDuration)
      ? `~${minDuration} min`
      : `${minDuration} - ${maxDuration} min`;
  }

  function _formatPlayerText(playerMin, playerMax) {
    if (!playerMax) {
      return `${playerMin} - unlimited players`;
    }

    if (playerMax === 1) {
      return 'Single Player';
    }

    return (playerMin === playerMax)
      ? `${playerMin} player`
      : `${playerMin} - ${playerMax} players`;
  }

})(window.App = window.App || {});
