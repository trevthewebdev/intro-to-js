'use strict';

(function(App) {

  App.GameFilters = {
    init: function init() {
      this.$selects = [].slice.call(document.getElementsByTagName('select'));
      this.bindEvents();
    },

    bindEvents: function bindEvents() {
      var self = this; // this === GameFilters

      this.$selects.forEach(function($select) {
        $select.addEventListener('change', self.onChange);
      });
    },

    onChange: function onChange(event) {
      var $target = event.target;
      var newFilter = {
        [$target.name]: $target.value
      };

      App.setState({
        GameListing: {
          filters: Object.assign({}, App.getState('GameListing').filters, newFilter)
        }
      });

      App.services
        .RequestGameListing(App.getState('GameListing').filters)
        .then(function(response) {
          /* update the global state */
          App.setState({
            GameListing: {
              games: response.data,
              total: response.meta.total
            }
          });

          /* tell the whole app we updated the games */
          dispatch('/app/listing/update');
        })

      /*
        console log the VALUE & the NAME of the select that was clicked on
      */
    }
  };

})(window.App = window.App || {});
