'use strict';

(function (App) {

  /* export this component to be used elsewhere */
  App.services = {
    RequestGameListing: function RequestGameListing(filters) {
      var queryParams = filtersToQueryParams(filters);

      return new Promise(function(resolve, reject) {
        axios
          .get(`https://overtureapp.com/api/v1/games${queryParams}`)
          .then(function (response) {
            return resolve(response.data);
          })
          .catch(function(error) {
            console.log('Request Games Error: ', error);
            return reject(error);
          })
      });
    }
  };

  function filtersToQueryParams(filters) {
    if (!filters) {
      return '';
    }

    var keys = Object.keys(filters);
    var queryParamsString = '?';

    return keys.reduce(function(prev, key, i) {
      var atEnd = i + 1 !== keys.length;
      if (filters[key]) {
        return `${prev}filter[${key}]=${filters[key]}${atEnd ? '&' : ''}`;
      }

      return prev;
    }, queryParamsString);
  }

})(window.App = window.App || {});
