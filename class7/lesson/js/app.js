'use strict';

(function(App) {
  var $drawerToggle = document.getElementById('drawer-toggle');

  /* start up all the app components */
  App.GameFilters.init();
  App.GameListing.init();
  App.GameSelections.init();

  /* do the intial request for games */
  App.services
    .RequestGameListing()
    .then(function(response) {

      /* update the global state */
      App.setState({
        GameListing: {
          games: response.data,
          total: response.meta.total
        }
      });

      /* tell the whole app we updated the games */
      dispatch('/app/listing/update');
    });

  /* bind some events that don't really fit in our other files */
  /* dispatchs a custom event to toggle the drawer */
  $drawerToggle.addEventListener('click', handleDrawerToggle);
  $drawerToggle.addEventListener('keyup', handleDrawerToggle);

  function handleDrawerToggle(event) {
    switch (event.which) {
      case 1:
      case 13:
      case 32:
        dispatch('app/drawer/toggle');
        break;
    }
  }

  window.addEventListener('/app/selections/update', function() {
    var selectionCount = App.getState('GameSelections').selections.length;
    $drawerToggle.innerHTML = `<i class="fa fa-star-o" aria-hidden="true"></i> <span class="show-for-sr">Games Selected:</span> ${selectionCount}`;
  });

})(window.App = window.App || {});
