'use strict';

(function(App) {

  /* export this component to be used elsewhere */
  App.GameFilters = {
    init: function init() {
      this.$selects = [].slice.call(document.getElementsByTagName('select'));
      this.bindEvents();
    },

    bindEvents: function bindEvents() {
      var self = this;

      this.$selects.forEach(function($elem) {
        $elem.addEventListener('change', self.onChange.bind(self));
      });
    },

    onChange: function onSelect(event) {
      var $target = event.target;
      var filters = {
        [$target.name]: $target.value
      };

      App.setState({
        GameListing: {
          filters: Object.assign({}, App.getState('GameListing').filters, filters)
        }
      });

      App.services
        .RequestGameListing(App.getState('GameListing').filters)
        .then(function(response) {

          /* update the global state */
          App.setState({
            GameListing: {
              games: response.data,
              total: response.meta.total
            }
          });

          /* tell the whole app we updated the games */
          dispatch('/app/listing/update');
        });
    }
  };

})(window.App = window.App || {});