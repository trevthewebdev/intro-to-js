'use strict';

(function () {

    // contructor function
    function Car(details) {
        this.make = details.make;
        this.model = details.model;
        this.color = details.color;

        // start up the car
        this.init(details.id);
    }

    Car.prototype = {
        init: function init(id) {
            console.log('this in Init: ', this);
            this.$button = document.getElementById(id);
            this.$button.addEventListener('click', this.brake.bind(this));
        },

        drive: function drive() {
            console.log('Driving the ' + this.make + ' ' + this.model);
        },

        brake: function brake() {
            console.log('this in Brake: ', this);
            console.log('Stopping the ' + this.color + ' ' + this.make);
        }
    }

    var car1 = new Car({
        make: 'Scion',
        model: 'xB',
        color: 'white',
        id: 'car1'
    });

    var car2 = new Car({
        make: 'Toyota',
        model: 'Carolla',
        color: 'Dark Grey',
        id: 'car2'
    });

    car1.drive();
    car2.drive();

    // car2.hello = function(name) {
    //     console.log('hello, ', name);
    // }

    // car2.hello('Trevor');
    // car1.hello('Other guy');

    // console.log(car1);
    // console.log(car2);

})();
