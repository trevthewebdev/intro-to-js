'use strict';

(function(App) {
    App.state = {
        GameListing: {
            games: [], // list of games
            total: 0,
            filters: {}
        },

        GameSelections: {
            count: 0
        }
    };

    App.GameListing.init();

    var $drawerToggle = document.getElementById('drawer-toggle');

    function handleDrawerToggle(event) {
        switch(event.which) {
            case 1:
            case 13:
            case 32:
                dispatch('app/drawer/toggle');
            break;
        }
    }
    
    $drawerToggle.addEventListener('click', handleDrawerToggle);
    $drawerToggle.addEventListener('keyup', handleDrawerToggle);

    // go get the games
    App.services
        .RequestGameListing()
        .then(function(response) {
            App.state.GameListing = Object.assign({}, App.state.GameListing, {
                games: response.data,
                total: response.meta.total
            });
            
            setTimeout(function() {
                dispatch('/app/listing/update', { detail: response.data });
            }, 2000);
        });

})(window.App = window.App || {});
