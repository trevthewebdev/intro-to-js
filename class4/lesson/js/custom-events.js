'use strict';

(function () {
    window.addEventListener('so-much-custom-very-much-wow', function(event) {
        console.log(event);
        console.log('Details for custom event fired: ', event.detail);
    });

    var event = new CustomEvent('so-much-custom-very-much-wow', {
        detail: 'This is some custom data' // whatever things you want
    });

    window.dispatchEvent(event);

})();
