'use strict';

(function (App) {

  /* export this component to be used elsewhere */
  App.services = {
    RequestGameListing: function RequestGameListing() {
      return new Promise(function(resolve, reject) {
        axios
          .get('https://overtureapp.com/api/v1/games')
          .then(function (response) {
            return resolve(response.data);
          })
          .catch(function(error) {
            console.log('Request Games Error: ', error);
            return reject(error);
          })
      });
    }
  };

})(window.App = window.App || {});
