'use strict';

(function () {
  /* listen for the custom event */
  window.addEventListener('so-much-custom-very-much-wow', function(event) {
    console.log('Details from custom event fired: ', event.detail);
  })

  /* create a new custom event */
  var event = new CustomEvent('so-much-custom-very-much-wow', { detail: 'This is some custom data' });

  /* fire / dispatch the new custom event */
  window.dispatchEvent(event);

})();
