'use strict';

(function(App) {
  var $drawerToggle = document.getElementById('drawer-toggle');

  /* global application state */
  App.state = {
    GameListing: {
      games: [], // the games that came back from the api
      total: 0, // total count of games that exist in the system
      filters: {} // current filters being applied
    },

    GameSelections: {
      count: 0 // count of games we've selected
    }
  };

  /* start up all the app components */
  // App.GameFilters.init();
  App.GameListing.init();
  App.GameSelections.init();

  /* do the intial request for games */
  App.services
    .RequestGameListing()
    .then(function (response) {
      App.state.GameListing = Object.assign({}, App.state.GameListing, {
        games: response.data,
        total: response.total
      });

      /* tell the whole app we updated the games */
      dispatch('/app/listing/update');
    });


  /* bind some events that don't really fit in our other files */
  /* dispatchs a custom event to toggle the drawer */
  $drawerToggle.addEventListener('click', handleDrawerToggle);
  $drawerToggle.addEventListener('keyup', handleDrawerToggle);

  function handleDrawerToggle(event) {
    switch (event.which) {
      case 1:
      case 13:
      case 32:
        dispatch('app/drawer/toggle');
        break;
    }
  }

})(window.App = window.App || {});
