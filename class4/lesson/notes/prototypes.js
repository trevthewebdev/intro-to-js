'use strict';

(function () {

  // constructor function
  function Car(details) {
    this.make = details.make;
    this.model = details.model;
    this.color = details.color;
    this.init(details.id);
  }

  // applied to all the cars
  Car.prototype = {
    init: function init(id) {
      var self = this;
      this.$button = document.getElementById(id);

      /* attach event listenr */
      this.$button.addEventListener('click', self.brake.bind(this));
    },

    drive: function drive() {
      console.log('Driving the ' + this.make + ' ' + this.model);
    },

    brake: function brake() {
      console.log('Stopping the ' + this.color + ' ' + this.make);
    }
  }

  var car1 = new Car({
    make: 'Scion',
    model: 'xB',
    color: 'white',
    id: 'car1'
  });

  var car2 = new Car({
    make: 'Toyota',
    model: 'Carolla',
    color: 'Dark Grey',
    id: 'car2'
  });

  car1.drive();
  car2.drive();

})();
