'use strict';

(function (App) {

  var GameListing = {
    init: function () {
      this.$container = document.getElementById('game-list');
      this.bindEvents();
    },

    bindEvents: function () {
      var self = this;

      window.addEventListener('/app/listing/update', function(data) {
        /* visually update the listing */
        _generateListHtml(self.$container, App.getState('GameListing').games);
      });
    }
  };

  /* export this component to be used elsewhere */
  App.GameListing = GameListing;


  /* _generateListHtml: will take an array of games and generate the listing in html */
  function _generateListHtml($container, gamesArray) {
    var $fragment = document.createDocumentFragment();

    gamesArray.forEach(function (game, index) {
      var $li = document.createElement('li');

      $li.classList.add('cell', 'medium-4', 'large-3');
      $li.innerHTML = (`
        <div class="card--game">
          <figure>
            <img src="https://placehold.it/400x250" alt="" />
          </figure>
          <div class="card__content">
            <h6 class="card__title">${game.name}</h6>
            <p>${game.description || 'Spooky scarey skeletons are doing things in their sleep...'}</p>
            <ul class="no-bullet">
              <li>${_formatTimeText(game.min_duration, game.max_duration)}</li>
              <li>${_formatPlayerText(game.player_min, game.player_max)}</li>
            </ul>
          </div>
          <div class="card__circle-actions">
            <button><i class="fa"></i></button>
          </div>
        </div>
      `);

      $fragment.appendChild($li);
    });

    $container.appendChild($fragment);
  }

  function _formatTimeText(minDuration, maxDuration) {
    return (minDuration === maxDuration)
      ? `~${minDuration} min`
      : `${minDuration} - ${maxDuration} min`;
  }

  function _formatPlayerText(playerMin, playerMax) {
    if (!playerMax) {
      return `${playerMin} - unlimited players`;
    }

    if (playerMax === 1) {
      return 'Single Player';
    }

    return (playerMin === playerMax)
      ? `${playerMin} player`
      : `${playerMin} - ${playerMax} players`;
  }

})(window.App = window.App || {});
