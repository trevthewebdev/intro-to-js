'use strict';

(function () {
  var promise = new Promise(function (resolve, reject) {
    var num = randomNum(1, 10);

    if (num > 2) {
      return resolve('whatever I want');
    }

    return reject(new Error('Something went wrong'));
  });

  /* Example of a promise chain */
  promise
    .then(function (value) {
      // here you can do something with the value
      console.log(value);
      console.log('\n ----');
    })
    .catch(function (error) {
      // here an error occured and needs to be dealt with
      console.log(error);
      console.log('\n ----');
    });

  /* One cheese burger please! */
  var cardDeclined = false;
  placeOrder()
    .then(cookPatty)
    .then(assembleBurger)
    .then(function (order) {
      console.log('Ding! Burger Done!', order);
    })
    .catch(function (error) {
      console.error(error);
    });

  /* While cheese burger is being made */
  if (!cardDeclined) {
    console.log('Getting napkins');

    setTimeout(function () {
      console.log('Filling drink cup');
    }, 2100);
  }


  /* Functions for making a cheese burger */
  function placeOrder() {
    return new Promise(function (resolve, reject) {
      var num = randomNum(1, 10);
      var orderNumber = randomNum(120, 300);

      if (num < 3) {
        cardDeclined = true;
        return reject(new Error('Card declined'));
      }

      console.log('Order placed! Starting burger...');
      return resolve(orderNumber);
    });
  }

  function cookPatty(orderNumber) {
    return new Promise(function (resolve, reject) {
      var order = {
        orderNumber: orderNumber,
        burger: {
          patty: 'beef'
        }
      };

      setTimeout(function () {
        console.log('Patty has been cooked!');
        return resolve(order);
      }, 2000);
    });
  }

  function assembleBurger(order) {
    return new Promise(function (resolve) {
      order.burger.cheese = 'cheddar';
      order.burger.condiments = ['katchup', 'mayonnaise', 'mustard'];

      setTimeout(function () {
        console.log('Burger assembled');
        return resolve(order);
      }, 1000);
    });
  }

})();
