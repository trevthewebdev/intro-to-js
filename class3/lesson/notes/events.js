'use strict';

(function () {
  /* Events, are not bound to scope or files, the window, document, or any element can listen for events to happen */
  var $cartCountButton = document.getElementById('cart-toggle');

  /* Example of an event listening to clicks on the page */
  document.addEventListener('click', function (event) {
    console.log('On Document Click was Triggered');

    /* Every event listener callback function receives an event argument */
    // console.log(event);
  });

  /* Example of multiple event listeners on a single element */
  function handleCartButton(event) {
    /* Every key has a unique number assigned to it, under the property event.which */
    console.log(event.which);

    /* We can check for which key or mouse button was pressed */
    switch (event.which) {
      case 1: // left mouse button click
      case 13: // enter
      case 32: // space
        console.log('This should trigger the cart open / toggle');
        break;
    }
  }

  $cartCountButton.addEventListener('click', handleCartButton)
  $cartCountButton.addEventListener('keypress', handleCartButton)

})();
