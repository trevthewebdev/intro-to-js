'use strict';

(function () {
  axios
    .get('https://overtureapp.com/api/v1/games')
    .then(function (result) {
      console.log(result);
    })
    .catch(function (error) {
      console.log(error);
    });

  axios
    .get('https://overtureapp.com/api/v1/games/593783dc5ee35676febbbf98')
    .then(function (result) {
      console.log(result);
    })
    .catch(function (error) {
      console.log(error);
    });

  axios
    .get('https://overtureapp.com/api/v1/games/lords-of-waterdeep')
    .then(function (result) {
      console.log(result);
    })
    .catch(function (error) {
      console.log(error);
    });

})();
