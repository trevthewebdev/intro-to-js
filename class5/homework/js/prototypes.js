'use strict';

var expect = chai.expect;

(function () {

  /**
   * Person Constructor
   * 1. Create a "Person" constructor function that takes 1 parameter, an object that has the following properties
   *    - hairColor: {string}
   *    - eyeColor: {string}
   *    - height:   {number representing feet}
   *    - fullName: {string}
   * 2. In the newly create Person constuctor attach all the properties to this instance of the Person. Remember back to the Car constructor
   */


  /**
   * Prototypes & Sharing Methods
   * 1. Create a new function called "walk" and attach it to the Person prototype, it should return the string "{fullName} is walking..."
   *    where fullName is the newly created Person's full name
   */


  /**
   * Adding Person Specific Methods
   * 1. Create new variable called "thePerson", set "thePerson" equal to a new instance of Person. Add a new method/function to "thePerson" called "talk".
   *    "talk" should return "I am talking...". This method should be specific to only "thePerson" and should not apply to all Persons
   */


  /* TESTS: DO NO TOUCH */
  describe('Prototypes', function () {

    describe('Person Constructor', function () {
      it('should be a function', function () {
        expect(Person instanceof Function).to.equal(true);
      });

      it('should have all the correct properties', function() {
        var person = new Person({
          hairColor: 'Brown',
          eyeColor: 'Brown',
          height: 6,
          fullName: 'Trevor Pierce'
        });

        expect(person.hairColor).to.equal('Brown');
        expect(person.eyeColor).to.equal('Brown');
        expect(person.height).to.equal(6);
        expect(person.fullName).to.equal('Trevor Pierce');
      });
    });

    describe('Prototype', function() {
      it('should allow for multiple instances of a Person to walk using only 1 function', function() {
        var person1 = new Person({
          hairColor: 'Brown',
          eyeColor: 'Brown',
          height: 6,
          fullName: 'Trevor Pierce'
        });

        var person2 = new Person({
          hairColor: 'Brown',
          eyeColor: 'Blue',
          height: 6,
          fullName: 'Shae Karl'
        });

        expect(person1.hasOwnProperty('walk')).to.equal(false);
        expect(person1.walk()).to.equal('Trevor Pierce is walking...');
        expect(person2.hasOwnProperty('walk')).to.equal(false);
        expect(person2.walk()).to.equal('Shae Karl is walking...');
      });
    });

    describe('Instance Specific Methods', function() {
      it('should only attach the talk method to "thePerson" not all created Persons', function() {
        var person1 = new Person({
          hairColor: 'Brown',
          eyeColor: 'Brown',
          height: 6,
          fullName: 'Trevor Pierce'
        });

        expect(thePerson.hasOwnProperty('talk')).to.equal(true);
        expect(thePerson.talk()).to.equal('I am talking...');

        expect(person1.hasOwnProperty('talk')).to.equal(false);
        expect(person1.talk).to.equal(undefined);
      });
    });

  });
})();
