'use strict';

var expect = chai.expect;

(function () {

  /**
   * Promises
   * 1. Create a promise and save it to the variable called promise below.
   */
  var promise;


  /**
   * Promises in functions
   * 1. Create a function called "goodPromise", it should take 1 parameter called "value". The function should return a promise that successfully returns the value that was passed in as the parameter
   * 2. Create a function called "badPromise". The function should return a promise that fails and returns the string "oops, something went wrong"
   */

  /* TESTS: DO NO TOUCH */
  describe('Promises', function() {

    describe('New Promise', function() {
      it('should be a promise', function() {
        expect(promise instanceof Promise).to.equal(true);
      });
    });


    describe('goodPromise()', function() {
      it('should return a promise', function() {
        expect(goodPromise instanceof Function).to.equal(true);
        expect(goodPromise() instanceof Promise).to.equal(true);
      });

      it('should return the same value as was passed in', function() {
        goodPromise('the thing')
          .then(function(value) {
            expect(value).to.equal('the thing');
          });

        goodPromise('another thing')
          .then(function(value) {
            expect(value).to.equal('another thing');
          });
      });
    });

    describe('badPromise()', function () {
      it('should return a promise', function() {
        expect(badPromise instanceof Function).to.equal(true);
        expect(badPromise() instanceof Promise).to.equal(true);
      });

      it('should return "oops, something went wrong"', function () {
        badPromise()
          .then(function(value) {
            expect(value).to.equal(undefined);
          })
          .catch(function(value) {
            expect(value).to.equal('oops, something went wrong');
          });
      });
    });

  });
})();
