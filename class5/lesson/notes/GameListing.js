'use strict';

(function (App) {

  /* export this component to be used elsewhere */
  App.GameListing = {
    init: function init() {
      this.$container = document.getElementById('game-list');
      this.bindEvents();
    },

    render: function render(html) {
      this.$container.appendChild(html);
    },

    bindEvents: function bindEvents() {
      var self = this;
      var $starButtons = document.querySelectorAll('.card__circle-actions button');

      window.addEventListener('/app/listing/update', function(data) {
        var games = App.getState('GameListing').games;
        var html = _generateListHtml(games);

        /* visually update the listing */
        self.render(html);
      });

      $starButtons.forEach(function($button) {
        $button.addEventListener('click', self.handleAddToSelection.bind(self));
        $button.addEventListener('keyup', self.handleAddToSelection.bind(self));
      });
    },

    handleAddToSelection: function handleAddToSelection(event) {
      console.log('oh dear');
      switch (event.which) {
        case 1:
        case 13:
        case 32:
          dispatch('app/game/add', {
            detail: event.target.data.game_id
          });
          break;
      }
    }
  };

  /* _generateListHtml: will take an array of games and generate the listing in html */
  function _generateListHtml($container, gamesArray) {
    var $fragment = document.createDocumentFragment();

    gamesArray.forEach(function (game, index) {
      var $li = document.createElement('li');

      $li.classList.add('cell', 'medium-4', 'large-3');
      $li.innerHTML = (`
        <div class="card--game">
          <figure>
            <img src="https://placehold.it/400x250" alt="" />
          </figure>
          <div class="card__content">
            <h6 class="card__title">${game.name}</h6>
            <p>${game.description || 'Spooky scarey skeletons are doing things in their sleep...'}</p>
            <ul class="no-bullet">
              <li>${_formatTimeText(game.min_duration, game.max_duration)}</li>
              <li>${_formatPlayerText(game.player_min, game.player_max)}</li>
            </ul>
          </div>
          <div class="card__circle-actions">
            <button><i class="fa"></i></button>
          </div>
        </div>
      `);

      $fragment.appendChild($li);
    });

    return $fragment;
  }

  function _formatTimeText(minDuration, maxDuration) {
    return (minDuration === maxDuration)
      ? `~${minDuration} min`
      : `${minDuration} - ${maxDuration} min`;
  }

  function _formatPlayerText(playerMin, playerMax) {
    if (!playerMax) {
      return `${playerMin} - unlimited players`;
    }

    if (playerMax === 1) {
      return 'Single Player';
    }

    return (playerMin === playerMax)
      ? `${playerMin} player`
      : `${playerMin} - ${playerMax} players`;
  }

})(window.App = window.App || {});
