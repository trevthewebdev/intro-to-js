'use strict';

(function(App) {
  var _state = {
    GameListing: {
      games: [], // the games that came back from the api
      total: 0, // total count of games that exist in the system
      filters: {} // current filters being applied
    },

    GameSelections: {
      maximum: 3, // default maximum
      count: 0, // count of games we've selected
      selections: [] // games the user has selected
    }
  };

  /* pulls the entire state, or just the a specifc part of it */
  App.getState = function(key) {
    if (!key) {
      return _state;
    }

    return _state[key];
  }

  /* updates the state */
  App.setState = function(update) {
    _state = _.merge({}, _state, update);
  }

})(window.App = window.App || {});
