'use strict';

(function (App) {

  var GameSelections = {
    init: function init() {
      this.$container = document.getElementById('selection-drawer');
      this.bindEvents();
    },

    bindEvents: function bindEvents() {
      var self = this;

      /* events to toggle the "selection drawer" visibility */
      window.addEventListener('app/drawer/toggle', self.toggleDrawer.bind(self));
      window.addEventListener('keyup', function(event) {
        switch(event.which) {
          case 27:
            self.toggleDrawer(false);
            break;
        }
      });

      /* when the user adds a game to their selections */
      window.addEventListener('app/game/add', function(event) {
        var game = App.state.GameListing.games.find(function(game) {
          return event.detail === game._id;
        });

        console.log('hey dude');

        if (game) {
          self.games.push(game);
        }
      });
    },

    toggleDrawer: function toggleDrawer(action) {
      var shouldOpen = (
        typeof action === 'boolean'
          ? action
          : !App.state.GameSelections.isOpen
      );

      if (shouldOpen) {
        this.$container.classList.add('is-open');
      } else {
        this.$container.classList.remove('is-open');
      }

      /* update global state */
      App.state.GameSelections.isOpen = shouldOpen;
    }
  };

  /* export this component to be used elsewhere */
  App.GameSelections = GameSelections;

})(window.App = window.App || {});
