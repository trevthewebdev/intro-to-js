var expect = chai.expect;

(function() {
  var key1 = 'value1';
  var key2 = 'value2';
  var obj1 = {
    someProp: 'some value'
  };
  var fruit = {
    classification: 'Apple'
  };


  /**
   * Object Assignment
   * 1. Write a function that takes 3 arguments "object", "key", "value" adds the passed in key & value to the passed in object
   *    Hint: there are two different types of object "notations" for accessing object, dot notation & array notation
   * 2. Add the following properies and values to the fruit object
   *    - type = Honey Crisp
   *    - sweetness = 9
   *    - color red-yellow
   */

  function assignKey(object, key, value) {
    object[key] = value;
  }

  fruit.type = 'Honey Crisp';
  fruit.sweetness = 9;
  fruit.color = 'red-yellow';


  /**
   * Object Copy
   * 1. Use a native javascript function to make a copy of the fruit object & save it to a variable called "gala"
   */
  
  var gala = Object.assign({}, fruit);

  describe('Objects', function() {
    describe('function assignKey', function() {
      it('should take obj1 & assign key1 as a key to obj1', function() {
        var randoObj = {};

        assignKey(obj1, 'key1', key1);
        assignKey(randoObj, 'something', 'someone');

        expect(obj1).to.have.property('key1', 'value1');
        expect(randoObj).to.have.property('something', 'someone');
      });
    });

    describe('fruit', function() {
      it('should have 4 properties on it describing an apple', function() {
        expect(fruit).to.deep.equal({
          classification: 'Apple',
          type: 'Honey Crisp',
          sweetness: 9,
          color: 'red-yellow'
        });
      });

      it('should be a copy of fruit without chaning the values of the original', function() {
        gala.type = 'Gala';

        expect(gala).to.deep.equal({
          classification: 'Apple',
          type: 'Gala',
          sweetness: 9,
          color: 'red-yellow'
        });

        expect(fruit).to.deep.equal({
          classification: 'Apple',
          type: 'Honey Crisp',
          sweetness: 9,
          color: 'red-yellow'
        });
      });
    });
  });
})();
