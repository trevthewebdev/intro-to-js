var expect = chai.expect;

(function () {

  // write a function that reverses any given string
  function wordReverse(str) {
    return str.split('').reverse().join('');
  }

  // write a function that uppercases every other letter in a string
  function camelCase(phrase) {
    return (
      phrase
        .toUpperCase()
        .split('')
        .map(function(letter, index) {
          if (index % 2 == 0) {
            return letter.toLowerCase();
          } else {
            return letter;
          }
        })
        .join('')
    );
  }

  describe('BONUS: Strings ', function () {

    it('should reverse any given word', function () {
      var word = 'reverseme';
      expect(wordReverse(word)).to.equal('emesrever');
    });

    it('should camel case every other letter', function () {
      var word = 'this should camel case';
      expect(camelCase(word)).to.equal('tHiS ShOuLd cAmEl cAsE');
    });

  });
})();
