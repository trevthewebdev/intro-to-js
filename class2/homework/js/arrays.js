var expect = chai.expect;

(function() {
  var fruits = ['apple', 'orange', 'banana', 'mango', 'kiwi'];
  var markers = [{
    brand: 'Expo',
    color: 'green',
    size: 5
  }, {
    brand: 'Expo',
    color: 'red',
    size: 5
  }, {
    brand: 'Prisma Color',
    color: 'red',
    size: 5.5
  }, {
    brand: 'Expo',
    color: 'blue',
    size: 5
  }, {
    brand: 'Expo',
    color: 'black',
    size: 5
  }, {
    brand: 'Prisma Color',
    color: 'green',
    size: 5.5
  }];

  /**
   * Push
   * 1. Using the FRUITS array, add 2 new fruits... strawberry & blueberry
   */
   fruits.push('strawberry', 'blueberry');

  /**
   * Splice
   * 1. Using the FRUITS array, remove the banana from the array
   */
  fruits.splice(2, 1);

  /**
   * Filter
   * 1. Using the MARKERS array, create a new array with only Prisma Color marker objects in it
   * 2. Using the MARKERS array, create a new array with only Expo marker objects in it
   */

  var prismaColorMarkers = markers.filter(function(marker) {
    if (marker.brand === 'Prisma Color') {
      return marker;
    }
  });

  var expoMarkers = markers.filter(function(marker) {
    if (marker.brand === 'Expo') {
      return marker;
    }
  });


  /**
   * Map
   * 1. Using the MARKERS array, create a new array with the *size* properties of each marker converted to centimeters
   *    Note: 1 inch is about 2.54 cenimeters
   * 2. Using the MARKERS array, create a new array with the brand *EXPO* changed to *Color Master*
   *
   */

  var markersWithUnitChange = markers.map(function(marker) {
    var newMarker = Object.assign({}, marker, { size: marker.size * 2.54 });
    return newMarker;
  });

  var expoMarkersNameChange = markers.map(function(marker) {
    var newMarker;
    
    if (marker.brand === 'Expo') {
      newMarker = Object.assign({}, marker, { brand: 'Color Master' });
      return newMarker;
    }

    return marker;
  });


  /**
   * Find
   * 1. Using the MARKERS array, find the *red prisma color* marker
   * 2. Using the FRUITS array, find the mango
   * 3. Using the FRUITS array, find the dragon fruit
   */

  
  var redPrismaColor = markers.find(function(marker) {
    if (marker.brand === 'Prisma Color' && marker.color === 'red') {
      return marker;
    }
  });

  var mango = fruits.find(function(fruit) {
    if (fruit === 'mango') {
      return fruit;
    }
  });
  var dragonFruit = fruits.find(function(fruit) {
    if (fruit === 'dragon fruit') {
      return fruit;
    }
  });

  /* TESTS: DO NOT TOUCH THESE */
  describe('Arrays', function () {
    describe('Push / Slice', function () {
      it('should be an array with length 6', function () {
        expect(fruits).to.have.length(6);
      });

      it('should contain a strawberry & blueberry', function () {
        expect(fruits).to.be.an('array').that.does.include('strawberry');
        expect(fruits).to.be.an('array').that.does.include('blueberry');
      });

      it('should not contain a banana', function () {
        expect(fruits).to.be.an('array').that.does.not.include('banana');
      });
    });

    describe('Filter', function () {
      it('should be an array containing only prisma color markers', function () {
        expect(prismaColorMarkers).to.be.an('array').with.length(2);
        expect(prismaColorMarkers[1]).to.have.property('brand', 'Prisma Color');
      });

      it('should be an array containing only expo markers', function () {
        expect(expoMarkers).to.be.an('array').with.length(4);
        expect(expoMarkers[2]).to.have.property('brand', 'Expo');
      });
    });

    describe('Map', function () {
      it('should convert marker size from inches to centimeters', function () {
        expect(markersWithUnitChange[1]).to.have.property('size', 12.7);
      });

      it('should change the name of all expo markers to "Color Master"', function () {
        expect(expoMarkersNameChange[1]).to.have.property('brand', 'Color Master');
      });
    });

    describe('Find', function () {
      it('should find 1 object that deeply equals the prisma color red', function () {
        expect(redPrismaColor).to.deep.equal({
          brand: 'Prisma Color',
          color: 'red',
          size: 5.5
        });
      });

      it('should find the word mango', function () {
        expect(mango).to.equal('mango');
      });

      it('should return undefined', function () {
        expect(dragonFruit).to.equal(undefined);
      });
    });
  });
})()

