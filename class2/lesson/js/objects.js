(function() {
    var couch = {
        color: 'green'
    };
    var propName = 'width';

    couch.width = '3ft';
    couch['height'] = '2.5ft';

    console.log(couch.propName); // will be undefined
    console.log(couch[propName]); // will be 3ft

    console.log(couch);

    // make a copy of the object
    var couchCopy = Object.assign({}, couch);
    couchCopy.width = '5ft';

    console.log(couch);
    console.log(couchCopy);

})();
