(function () {
    var furniture = ['table', 'chair', 'night stand', 'table'];
    
    furniture.push('lamp');
    furniture.push('couch', 'rug');

    console.log('furniture', furniture);

    // make a copy of the array
    var newFurniture = furniture.slice();

    // remove items from the array
    newFurniture.splice(1, 2);

    console.log('newFurniture', newFurniture);
    console.log('furniture', furniture);

    var tables = furniture.filter(function(value, index) {
        if (value === 'table') {
            return value;
        }
    });

    console.log('tables', tables);

    var transform = furniture.map(function(value, index) {
        return value + index + 'lamp';
    });

    console.log('transform', transform);

    var nightStand = furniture.find(function(value) {
        if (value === 'night stand') {
            return value;
        }
    });

    console.log('find', nightStand);

})();
