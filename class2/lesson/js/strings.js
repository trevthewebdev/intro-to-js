(function () {
    var coolguy = 'The Dude';
    console.log(coolguy.substring(4));
    console.log(coolguy.substring(4, 5));

    var coolgirl = 'The Dudette';
    console.log(coolgirl.replace('The ', ''));

    var anotherString = 'this should be split';
    console.log(anotherString.split(''));
    console.log(anotherString.split(' '));
})()
