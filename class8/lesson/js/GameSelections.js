'use strict';

(function (App) {

  var GameSelections = {
    init: function init() {
      this.$container = document.getElementById('selection-drawer');
      this.$containerList = document.getElementById('drawer-game-list');
      this.bindEvents();
    },

    render: function render(html) {
      this.$containerList.innerHTML = '';
      this.$containerList.appendChild(html);
    },

    bindEvents: function bindEvents() {
      var self = this;

      this.$containerList.addEventListener('click', function(event) {
        var $elem = event.target;

        if ($elem.classList.contains('remove-item-button')) {
          var gameId = $elem.dataset.gameId;

          // get the selections from the state & make a copy of it to manipulate
          var state = App.getState('GameSelections');
          var selections = state.selections.slice();

          // find the index of what we want to remove
          var index = selections.findIndex(function(id) {
            if (id === gameId) {
              return true;
            }
          });

          // remove it from the copy
          selections.splice(index, 1);

          // set the state with the new selections
          App.setState({
            GameSelections: {
              selections: selections
            }
          });

          // tell the rest of the app there was an update
          dispatch('/app/selections/update');
        }
      });

      /* events to toggle the "selection drawer" visibility */
      window.addEventListener('app/drawer/toggle', self.toggleDrawer.bind(self));
      window.addEventListener('keyup', function(event) {
        switch(event.which) {
          case 27:
            self.toggleDrawer();
            break;
        }
      });

      /* when the user adds a game to their selections */
      window.addEventListener('/app/selections/update', function() {
        // update the count in the header
        // generate and add the html
        var state = App.getState();
        var listing = state.GameListing.games;
        var selections = state.GameSelections.selections;
        var games = selections.map(function(gameId) {
          var match = listing.find(function(game) {
            if (game._id === gameId) {
              return {
                _id: game._id,
                name: game.name
              };
            }
          });

          return match;
        });
        var html = _generateListHtml(games);

        /* visually update the listing */
        self.render(html);
      });
    },

    toggleDrawer: function toggleDrawer(action) {
      var hasClass = this.$container.classList.contains('is-open');

      if (hasClass) {
        this.$container.classList.remove('is-open');
      } else {
        this.$container.classList.add('is-open');
      }
    },

  };

  function _generateListHtml(gamesArray) {
    var $fragment = document.createDocumentFragment();

    gamesArray.forEach(function (game, index) {
      var $li = document.createElement('li');

      $li.classList.add('cell', 'medium-4', 'large-3', 'drawer-item');
      $li.innerHTML = (`
        <div class="media-object">
          <div class="media-object-section">
            <div class="thumbnail">
              <img src="http://placehold.it/50">
          </div>
          </div>
          <div class="media-object-section">
            <h6 class="subheader">${game.name}</h6>
          </div>
        </div>
        <a data-game-id="${game._id}" class="remove-item-button">x</a>
      `)

      $fragment.appendChild($li);
    });

    return $fragment;
  }

  /* export this component to be used elsewhere */
  App.GameSelections = GameSelections;

})(window.App = window.App || {});
