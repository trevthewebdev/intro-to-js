'use strict';

(function(App) {
  var $drawerToggle = document.getElementById('drawer-toggle');

  /* start up all the app components */
  App.GameFilters.init();
  App.GameListing.init();
  App.GameSelections.init();

  var cache = JSON.parse(localStorage.getItem('app-games'));

  /* if there are games in the cache and we are not passed the expire time */
  if (cache && new Date().getTime() <= cache.expires) {
    /* update the global state */
    App.setState({
      GameListing: {
        games: cache.games,
        total: cache.total
      }
    });

    /* tell the whole app we updated the games */
    dispatch('/app/listing/update');
  } else {
    App.services
      .RequestGameListing()
      .then(function(response) {

        /* update the global state */
        App.setState({
          GameListing: {
            games: response.data,
            total: response.meta.total
          }
        });

        /* tell the whole app we updated the games */
        dispatch('/app/listing/update');

        /* cache the games that came back */
        localStorage.setItem('app-games', JSON.stringify({
          games: response.data,
          total: response.meta.total,
          expires: new Date(new Date().getTime() + 1 * 60000).getTime() // expires after (1) minute
        }));
      });

  }

  /* bind some events that don't really fit in our other files */
  /* dispatchs a custom event to toggle the drawer */
  $drawerToggle.addEventListener('click', handleDrawerToggle);
  $drawerToggle.addEventListener('keyup', handleDrawerToggle);

  function handleDrawerToggle(event) {
    switch (event.which) {
      case 1:
      case 13:
      case 32:
        dispatch('app/drawer/toggle');
        break;
    }
  }

  window.addEventListener('/app/selections/update', function() {
    var selectionCount = App.getState('GameSelections').selections.length;
    $drawerToggle.innerHTML = `<i class="fa fa-star-o" aria-hidden="true"></i> <span class="show-for-sr">Games Selected:</span> ${selectionCount}`;
  });

})(window.App = window.App || {});
